package pl.edu.wat.simulations.marketsimulation.helpers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CustomerGenerationStrategyTest {
    GenerationStrategy instance;

    @Test
    public void shouldNeverGenerateForRandomWith0() throws Exception {
        // given
        instance = GenerationStrategyFactory.randomGenerationStrategy(0);
        // when
        // then
        for (int i = 0; i < 100; i++) {
            assertThat(instance.shouldGenerate(0)).isFalse();
        }
    }

    @Test
    public void shouldAlwaysGenerateIfRandomAnd1() throws Exception {
        // given
        instance = GenerationStrategyFactory.randomGenerationStrategy(1);
        // when
        // then
        for (int i = 0; i < 100; i++) {
            assertThat(instance.shouldGenerate(0)).isTrue();
        }
    }

    @Test
    public void shouldInvokeCustomGenerator() throws Exception {
        // given
        instance = GenerationStrategyFactory.customGenerationStrategy(time -> {
            return false;
        });
        // when
        // then
        for (int i = 0; i < 100; i++) {
            assertThat(instance.shouldGenerate(0)).isFalse();
        }
    }
}
