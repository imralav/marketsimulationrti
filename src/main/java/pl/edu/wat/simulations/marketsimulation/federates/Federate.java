package pl.edu.wat.simulations.marketsimulation.federates;

import hla.rti.RTIexception;

public interface Federate {
    void initiateFederate() throws RTIexception;
    void runFederate() throws RTIexception;
    void cleanUpFederate() throws RTIexception;
}
