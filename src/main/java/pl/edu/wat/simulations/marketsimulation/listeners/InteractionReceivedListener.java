package pl.edu.wat.simulations.marketsimulation.listeners;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.ReceivedInteraction;

public interface InteractionReceivedListener {

    public void notifyInteractionReceived(int interactionClass, ReceivedInteraction theInteraction, byte[] tag,
            LogicalTime theTime, EventRetractionHandle eventRetractionHandle);

}
