package pl.edu.wat.simulations.marketsimulation.federates.implementation;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.ambassadors.implementation.ManagerFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.federates.AbstractFederate;
import pl.edu.wat.simulations.marketsimulation.federates.Federate;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomInteraction;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomObject;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;
import pl.edu.wat.simulations.marketsimulation.listeners.AttributesUpdatedListener;

public class ManagerFederate extends AbstractFederate {
    private static final Logger logger = LoggerFactory.getLogger(ManagerFederate.class);
    private static final String PRZEPELNIONA = "przepelniona";

    private FomInteraction otworzKaseClassHandle;
    private FomObject checkoutClassHandle;
    private AbstractFederateAmbassador fedAmbassador;
    private Map<Integer, Boolean> overcrowdedCheckouts;
    private FomInteraction stopClassHandle;
    /**
     * a set of queue sizes, for which {@link ManagerFederate#areAllQueuesFilled()} has returned true in
     * {@link ManagerFederate#getAttributeUpdatedListener()}
     */
    private Set<Integer> filledQueues;

    public ManagerFederate(String federateName, String federationName, File fomFile)
            throws RTIinternalError, MalformedURLException {
        super(federateName, federationName, fomFile);
        overcrowdedCheckouts = new HashMap<>();
        filledQueues = new HashSet<>();
    }

    @Override
    public void runFederate() throws RTIexception {
        while (isRunning) {
            advanceTime(2);
            logger.info("Checking if all queues have been filled...");
            filledQueues = filledQueues.stream().map(queuesAmount -> {
                try {
                    logger.info("Queues have been filled when there were {} of them, sending OtworzKase interaction",
                            queuesAmount);
                    sendOtworzKaseInteraction();
                    return -1;
                } catch (Exception e) {
                    logger.error("Couldn't send OtworzKase interaction when {} queue(s) were filled: {}",
                            queuesAmount,
                            e.getMessage());
                    return queuesAmount;
                }
            }).filter(queuesAmount -> queuesAmount != -1).collect(Collectors.toSet());
            logger.info("Time advanced to {}", getFederateAmbassador().getFederateTime());
        }
    }

    public static void main(String[] args) {
        executeFederate(args);
    }

    @Override
    protected AbstractFederateAmbassador getFederateAmbassador() {
        return fedAmbassador;
    }

    @Override
    protected void prepareFederationAmbassador() {
        fedAmbassador = new ManagerFederateAmbassador();
        fedAmbassador.registerObjectInstanceCreatedListener((objectHandle, classHandle, objectName) -> {
            if (checkoutClassHandle.getClassHandle() == classHandle) {
                logger.info("New checkout created {}", objectHandle);
                overcrowdedCheckouts.put(objectHandle, false);
                objectToClassHandleMap.put(objectHandle, classHandle);
            }
        });
        fedAmbassador.registerAttributesUpdatedListener(getAttributeUpdatedListener());
        fedAmbassador.registerInteractionReceivedListener((int interactionClass, ReceivedInteraction theInteraction,
                byte[] tag, LogicalTime theTime, EventRetractionHandle eventRetractionHandle) -> {
            if (interactionClass == stopClassHandle.getClassHandle()) {
                logger.info("Stop interaction received");
                stop();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishAndSubscribe() throws RTIexception {
        otworzKaseClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.OtworzKase"));
        rtiAmbassador.publishInteractionClass(otworzKaseClassHandle.getClassHandle());

        checkoutClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Kasa"),
                new Pair<String, Class<?>>(PRZEPELNIONA, Boolean.class));
        rtiAmbassador.subscribeObjectClassAttributes(checkoutClassHandle.getClassHandle(),
                checkoutClassHandle.createAttributeHandleSet());

        stopClassHandle = prepareFomInteraction(rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.Stop"));
        rtiAmbassador.subscribeInteractionClass(stopClassHandle.getClassHandle());
    }

    private boolean isAttributeFilled(int attributeHandle, Class<?> typeFor) {
        return typeFor.getName().equalsIgnoreCase(Boolean.class.getName())
                && checkoutClassHandle.getNameFor(attributeHandle).equalsIgnoreCase(PRZEPELNIONA);
    }

    private void sendOtworzKaseInteraction() throws RTIinternalError, InteractionClassNotDefined,
            InteractionClassNotPublished, InteractionParameterNotDefined, FederateNotExecutionMember, SaveInProgress,
            RestoreInProgress, ConcurrentAccessAttempted {
        SuppliedParameters parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        rtiAmbassador.sendInteraction(otworzKaseClassHandle.getClassHandle(), parameters, generateTag());
    }

    private boolean areAllQueuesFilled() {
        return overcrowdedCheckouts.size() > 0 && overcrowdedCheckouts.entrySet().stream().allMatch(entry -> {
            return entry.getValue();
        });
    }

    private static void executeFederate(String[] args) {
        String federateName = "managerFederate";
        String federationName = AbstractFederateAmbassador.FEDERATION_NAME;
        String pathname = AbstractFederate.FOM_PATH;
        if (args.length > 0) {
            federateName = args[0];
        }
        if (args.length > 1) {
            federationName = args[1];
        }
        if (args.length > 2) {
            pathname = args[2];
        }
        try {
            logger.info("Creating federate");
            Federate federate = new ManagerFederate(federateName, federationName, new File(pathname));
            federate.initiateFederate();
            federate.runFederate();
            federate.cleanUpFederate();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RTIexception e) {
            e.printStackTrace();
        }
    }

    private AttributesUpdatedListener getAttributeUpdatedListener() {
        return (objectHandle, theAttributes, tag, theTime, retractionHandle) -> {
            Integer classHandle = objectToClassHandleMap.get(objectHandle);
            if (classHandle.equals(checkoutClassHandle.getClassHandle())) {
                logger.info("Checkout {} updated at federation time {}, checking if filled", objectHandle, theTime);
                for (int i = 0; i < theAttributes.size(); i++) {
                    try {
                        int attributeHandle = theAttributes.getAttributeHandle(i);
                        Class<?> typeFor = checkoutClassHandle.getTypeFor(attributeHandle);
                        if (isAttributeFilled(attributeHandle, typeFor)) {
                            boolean isFilled = EncodingHelpers.decodeBoolean(theAttributes.getValue(i));
                            logger.info("Queue {} filled: {}", objectHandle, isFilled);
                            overcrowdedCheckouts.put(objectHandle, isFilled);
                        }
                        if (areAllQueuesFilled()) {
                            filledQueues.add(overcrowdedCheckouts.size());
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
        };
    }
}
