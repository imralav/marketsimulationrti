package pl.edu.wat.simulations.marketsimulation.helpers;

import java.time.Duration;
import java.time.LocalTime;

public class StopWatch {
    private Duration duration;
    private LocalTime startTime;

    private StopWatch(Duration duration) {
        this.duration = duration;
        reset();
    }

    public static StopWatch createStopWatchForDuration(Duration duration) {
        return new StopWatch(duration);
    }

    public void reset() {
        this.startTime = LocalTime.now();
    }

    public Duration getTimePassed() {
        return Duration.between(startTime, LocalTime.now());
    }

    public boolean hasDurationPassed() {
        Duration timePassed = getTimePassed();
        return timePassed.compareTo(duration) >= 0;
    }

    public void blockUntilDurationPassed() {
        while (!hasDurationPassed()) {
        }
    }
}
