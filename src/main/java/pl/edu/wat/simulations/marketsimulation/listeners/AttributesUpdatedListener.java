package pl.edu.wat.simulations.marketsimulation.listeners;

import hla.rti.EventRetractionHandle;
import hla.rti.ReflectedAttributes;

public interface AttributesUpdatedListener {

    public void notifyAttributesUpdated(int theObject, ReflectedAttributes theAttributes, byte[] tag, Double time,
            EventRetractionHandle retractionHandle);

}
