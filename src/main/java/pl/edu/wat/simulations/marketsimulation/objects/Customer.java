package pl.edu.wat.simulations.marketsimulation.objects;

public class Customer {

    private Double oldFederateTime;
    private Double shoppingTime;
    private Double serviceTime;
    private boolean hasFinishedShopping;
    private boolean hasServiceFinished;
    private boolean priviledged;

    public Customer(double oldFederateTime, Double shoppingTime, Double serviceTime) {
        this.oldFederateTime = oldFederateTime;
        this.shoppingTime = shoppingTime;
        this.serviceTime = serviceTime;
        priviledged = false;
    }

    public double getOldFederateTime() {
        return oldFederateTime;
    }

    public Double getShoppingTime() {
        return shoppingTime;
    }

    public Double getServiceTime() {
        return serviceTime;
    }

    public void updateWithNewFederateTime(double newFederateTime) {
        if (shoppingTime != null) {
            this.hasFinishedShopping = newFederateTime - oldFederateTime >= shoppingTime;
        }
        if (serviceTime != null) {
            this.hasServiceFinished = newFederateTime - oldFederateTime >= serviceTime;
        }
    }

    public boolean hasFinishedShopping() {
        return hasFinishedShopping;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(oldFederateTime);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Customer other = (Customer) obj;
        if (Double.doubleToLongBits(oldFederateTime) != Double.doubleToLongBits(other.oldFederateTime))
            return false;
        return true;
    }

    public boolean hasServiceFinished() {
        return hasServiceFinished;
    }

    public void setOldFederateTime(double oldFederateTime) {
        this.oldFederateTime = oldFederateTime;
    }

    public boolean isPriviledged() {
        return priviledged;
    }

    public void setPriviledged(boolean priviledged) {
        this.priviledged = priviledged;
    }

    @Override
    public String toString() {
        return "Customer [oldFederateTime=" + oldFederateTime + ", shoppingTime=" + shoppingTime + ", serviceTime="
                + serviceTime + ", hasFinishedShopping=" + hasFinishedShopping + ", hasServiceFinished="
                + hasServiceFinished + ", priviledged=" + priviledged + "]";
    }
}
