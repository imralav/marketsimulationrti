package pl.edu.wat.simulations.marketsimulation.federates;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.function.Consumer;

import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.*;
import hla.rti.jlc.RTIambassadorEx;
import hla.rti.jlc.RtiFactoryFactory;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomInteraction;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomObject;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;

public abstract class AbstractFederate implements Federate {
    public static final int DEFAULT_ITERATIONS = 500;
    private static final Logger logger = LoggerFactory.getLogger(AbstractFederate.class);
    public static final String FOM_PATH = "src/main/resources/testfom.xml";

    protected RTIambassador rtiAmbassador;
    protected String federateName;
    protected String federationName;
    protected Map<Integer, Integer> objectToClassHandleMap;
    private List<Runnable> queuedTasks;
    protected boolean isRunning = true;

    public AbstractFederate(String federateName, String federationName, File fomFile)
            throws RTIinternalError, MalformedURLException {
        this.federateName = federateName;
        this.federationName = federationName;
        rtiAmbassador = prepareRTIAmbassador();
        createFederationExecution(federationName, fomFile.toURI().toURL());
        prepareFederationAmbassador();
        joinFederationExecution(federateName, federationName);
        objectToClassHandleMap = new HashMap<>();
        queuedTasks = new LinkedList<Runnable>();
    }

    public void stop() {
        isRunning = false;
    }

    protected void submitNewTask(Runnable task) {
        queuedTasks.add(task);
    }

    protected void executeAllQueuedTasks() {
        queuedTasks.forEach(Runnable::run);
        queuedTasks.clear();
    }

    private void joinFederationExecution(String federateName, String federationName) {
        try {
            logger.info("Joining {}", federationName);
            rtiAmbassador.joinFederationExecution(federateName, federationName, getFederateAmbassador());
        } catch (FederateAlreadyExecutionMember e) {
            logger.info(
                    "Federate " + federateName + " is already a member of " + federationName + " federation execution");
        } catch (FederationExecutionDoesNotExist e) {
            e.printStackTrace();
        } catch (SaveInProgress e) {
            e.printStackTrace();
        } catch (RestoreInProgress e) {
            e.printStackTrace();
        } catch (RTIinternalError e) {
            e.printStackTrace();
        } catch (ConcurrentAccessAttempted e) {
            e.printStackTrace();
        }
    }

    /**
     * announce a sync point to get everyone on the same page. if the point has already been registered, we'll get a
     * callback saying it failed, but we don't care about that, as long as someone registered it
     * 
     * @throws FederateNotExecutionMember
     * @throws SaveInProgress
     * @throws RestoreInProgress
     * @throws RTIinternalError
     * @throws ConcurrentAccessAttempted
     */
    protected void registerSynchronizationPointAndWaitForAnnouncement() throws FederateNotExecutionMember,
            SaveInProgress, RestoreInProgress, RTIinternalError, ConcurrentAccessAttempted {
        rtiAmbassador.registerFederationSynchronizationPoint(AbstractFederateAmbassador.READY_TO_RUN, null);
        logger.info("Registered sync point: {}, waiting for federation...", AbstractFederateAmbassador.READY_TO_RUN);
        // wait until the point is announced
        while (getFederateAmbassador().isAnnounced() == false) {
            rtiAmbassador.tick();
        }
    }

    protected void achieveSyncPoint() throws SynchronizationLabelNotAnnounced, FederateNotExecutionMember,
            SaveInProgress, RestoreInProgress, RTIinternalError, ConcurrentAccessAttempted {
        rtiAmbassador.synchronizationPointAchieved(AbstractFederateAmbassador.READY_TO_RUN);
        logger.info("Achieved sync point: {}, waiting for federation...", AbstractFederateAmbassador.READY_TO_RUN);
        while (getFederateAmbassador().isReadyToRun() == false) {
            rtiAmbassador.tick();
        }
    }

    protected abstract AbstractFederateAmbassador getFederateAmbassador();

    protected abstract void prepareFederationAmbassador();

    private void createFederationExecution(String federationName, URL url) {
        try {
            logger.info("Attempting to create a federation " + federationName + " from fed file " + url);
            rtiAmbassador.createFederationExecution(federationName, url);
            logger.info("Federation " + federationName + " created");
        } catch (FederationExecutionAlreadyExists e) {
            logger.info("Federation " + federationName + " already exists");
        } catch (CouldNotOpenFED e) {
            e.printStackTrace();
        } catch (ErrorReadingFED e) {
            e.printStackTrace();
        } catch (ConcurrentAccessAttempted e) {
            e.printStackTrace();
        } catch (RTIinternalError e) {
            e.printStackTrace();
        }
    }

    private RTIambassadorEx prepareRTIAmbassador() throws RTIinternalError {
        return RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
    }

    /**
     * WAIT FOR USER TO KICK US OFF So that there is time to add other federates, we will wait until the user hits enter
     * before proceeding. That was, you have time to start other federates. This method will block until the user
     * presses enter
     */
    protected void waitForUser() {
        logger.info(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            logger.error("Error while waiting for user input: {}", e.getMessage());
        }
    }

    protected void initiateObjects() throws RTIexception {
        // do nothing by default
        logger.info("Not initiating any objects");
    };

    /**
     * This method will attempt to enable the various time related properties for the federate
     */
    public void enableTimePolicy() throws RTIexception {
        // NOTE: Unfortunately, the LogicalTime/LogicalTimeInterval create code
        // is
        // Portico specific. You will have to alter this if you move to a
        // different RTI implementation. As such, we've isolated it into a
        // method so that any change only needs to happen in a couple of spots
        LogicalTime currentTime = convertTime(getFederateAmbassador().getFederateTime());
        LogicalTimeInterval lookahead = convertInterval(getFederateAmbassador().getFederateLookahead());

        ////////////////////////////
        // enable time regulation //
        ////////////////////////////
        this.rtiAmbassador.enableTimeRegulation(currentTime, lookahead);

        // tick until we get the callback
        while (getFederateAmbassador().isRegulating() == false) {
            rtiAmbassador.tick();
        }

        /////////////////////////////
        // enable time constrained //
        /////////////////////////////
        this.rtiAmbassador.enableTimeConstrained();

        // tick until we get the callback
        while (getFederateAmbassador().isConstrained() == false) {
            rtiAmbassador.tick();
        }
    }

    protected abstract void publishAndSubscribe() throws RTIexception;

    @Override
    public void cleanUpFederate() throws RTIexception {
        deleteObjects();
        resignFromFederation();
        attemptToDestroyFederation();
    }

    protected void deleteObjects() throws RTIexception {
        // do nothing by default
        logger.info("Not deleting any objects");
    };

    protected void resignFromFederation() throws FederateOwnsAttributes, FederateNotExecutionMember,
            InvalidResignAction, RTIinternalError, ConcurrentAccessAttempted {
        ////////////////////////////////////
        // 11. resign from the federation //
        ////////////////////////////////////
        rtiAmbassador.resignFederationExecution(ResignAction.NO_ACTION);
        logger.info("Resigned from Federation");
    }

    /**
     * NOTE: we won't die if we can't do this because other federates // remain. in that case we'll leave it for them to
     * clean up
     * 
     * @throws RTIinternalError
     * @throws ConcurrentAccessAttempted
     */
    protected void attemptToDestroyFederation() throws RTIinternalError, ConcurrentAccessAttempted {
        try {
            rtiAmbassador.destroyFederationExecution(federationName);
            logger.info("Destroyed Federation");
        } catch (FederationExecutionDoesNotExist dne) {
            logger.info("No need to destroy federation, it doesn't exist");
        } catch (FederatesCurrentlyJoined fcj) {
            logger.info("Didn't destroy federation, federates still joined");
        }
    }

    @Override
    public void initiateFederate() throws RTIexception {
        registerSynchronizationPointAndWaitForAnnouncement();
        waitForUser();
        achieveSyncPoint();
        enableTimePolicy();
        publishAndSubscribe();
        initiateObjects();
    }

    /**
     * As all time-related code is Portico-specific, we have isolated it into a single method. This way, if you need to
     * move to a different RTI, you only need to change this code, rather than more code throughout the whole class.
     */
    protected LogicalTime convertTime(double time) {
        // PORTICO SPECIFIC!!
        return new DoubleTime(time);
    }

    /**
     * Same as for {@link #convertTime(double)}
     */
    protected LogicalTimeInterval convertInterval(double time) {
        // PORTICO SPECIFIC!!
        return new DoubleTimeInterval(time);
    }

    /**
     * This method will request a time advance to the current time, plus the given timestep. It will then wait until a
     * notification of the time advance grant has been received.
     */
    protected void advanceTime(double timestep) throws RTIexception {
        // request the advance
        getFederateAmbassador().setAdvancing(true);
        LogicalTime newTime = convertTime(getFederateAmbassador().getFederateTime() + timestep);
        rtiAmbassador.timeAdvanceRequest(newTime);

        // wait for the time advance to be granted. ticking will tell the
        // LRC to start delivering callbacks to the federate
        while (getFederateAmbassador().isAdvancing()) {
            rtiAmbassador.tick();
        }
    }

    /**
     * This method will attempt to delete the object instance of the given handle. We can only delete objects we
     * created, or for which we own the privilegeToDelete attribute.
     */
    protected void deleteObject(int handle) throws RTIexception {
        rtiAmbassador.deleteObjectInstance(handle, generateTag());
    }

    protected double getLbts() {
        return getFederateAmbassador().getFederateTime() + getFederateAmbassador().getFederateLookahead();
    }

    protected byte[] generateTag() {
        return ("" + System.currentTimeMillis()).getBytes();
    }

    public FomObject prepareFomObject(int classHandle,
            @SuppressWarnings("unchecked") Pair<String, Class<?>>... attributeNamesAndEncodingFunctions) {
        FomObject object = new FomObject(classHandle);
        iterateArrayWhileDoing(attributeNamesAndEncodingFunctions, pair -> {
            try {
                object.addAttributeHandle(pair.getA(),
                        rtiAmbassador.getAttributeHandle(pair.getA(), classHandle),
                        pair.getB());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        return object;
    }

    public FomInteraction prepareFomInteraction(int classHandle,
            @SuppressWarnings("unchecked") Pair<String, Class<?>>... parameterNamesAndEncodingFunctions) {
        FomInteraction interaction = new FomInteraction(classHandle);
        iterateArrayWhileDoing(parameterNamesAndEncodingFunctions, pair -> {
            try {
                interaction.addAttributeHandle(pair.getA(),
                        rtiAmbassador.getParameterHandle(pair.getA(), classHandle),
                        pair.getB());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        return interaction;
    }

    private <T> void iterateArrayWhileDoing(T[] array, Consumer<? super T> action) {
        Arrays.stream(array).forEach(pair -> {
            action.accept(pair);
        });
    }
}
