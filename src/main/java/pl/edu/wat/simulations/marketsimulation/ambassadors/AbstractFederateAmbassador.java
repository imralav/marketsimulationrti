package pl.edu.wat.simulations.marketsimulation.ambassadors;

import java.util.ArrayList;
import java.util.List;

import org.portico.impl.hla13.types.DoubleTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.ReceivedInteraction;
import hla.rti.ReflectedAttributes;
import hla.rti.jlc.NullFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.listeners.AttributesUpdatedListener;
import pl.edu.wat.simulations.marketsimulation.listeners.InteractionReceivedListener;
import pl.edu.wat.simulations.marketsimulation.listeners.ObjectInstanceCreatedListener;
import pl.edu.wat.simulations.marketsimulation.listeners.ObjectInstanceRemovedListener;

public abstract class AbstractFederateAmbassador extends NullFederateAmbassador {
    private static final Logger logger = LoggerFactory.getLogger(AbstractFederateAmbassador.class);
    public static final String FEDERATION_NAME = "MarketFederation";
    public static final String READY_TO_RUN = "ReadyToRun";
    protected double federateTime = 0.0;
    protected double federateLookahead = 1.0;

    protected boolean isRegulating = false;
    protected boolean isConstrained = false;
    protected boolean isAdvancing = false;

    protected boolean isAnnounced = false;
    protected boolean isReadyToRun = false;
    protected List<ObjectInstanceCreatedListener> objectInstanceCreatedListeners = new ArrayList<>();
    protected List<ObjectInstanceRemovedListener> objectInstanceRemovedListeners = new ArrayList<>();
    protected List<AttributesUpdatedListener> attributesUpdatedListeners = new ArrayList<>();
    protected List<InteractionReceivedListener> interactionReceivedListeners = new ArrayList<>();

    /**
     * As all time-related code is Portico-specific, we have isolated it into a single method. This way, if you need to
     * move to a different RTI, you only need to change this code, rather than more code throughout the whole class.
     */
    protected double convertTime(LogicalTime logicalTime) {
        // PORTICO SPECIFIC!!
        return ((DoubleTime) logicalTime).getTime();
    }

    @Override
    public void synchronizationPointRegistrationFailed(String label) {
        logger.info("Failed to register sync point: " + label);
    }

    @Override
    public void synchronizationPointRegistrationSucceeded(String label) {
        logger.info("Successfully registered sync point: " + label);
    }

    @Override
    public void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction, byte[] tag,
            LogicalTime theTime, EventRetractionHandle eventRetractionHandle) {
        logger.trace("Received interaction of class {}", interactionClass);
        interactionReceivedListeners.forEach(listener -> {
            listener.notifyInteractionReceived(interactionClass, theInteraction, tag, theTime, eventRetractionHandle);
        });
    }

    @Override
    public final void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction, byte[] tag) {
        // just pass it on to the other method for printing purposes
        // passing null as the time will let the other method know it
        // it from us, not from the RTI
        receiveInteraction(interactionClass, theInteraction, tag, null, null);
    }

    @Override
    public void announceSynchronizationPoint(String label, byte[] tag) {
        logger.info("Synchronization point announced: " + label);
        if (label.equals(READY_TO_RUN))
            this.isAnnounced = true;
    }

    @Override
    public void federationSynchronized(String label) {
        logger.info("Federation Synchronized: " + label);
        if (label.equals(READY_TO_RUN))
            this.isReadyToRun = true;
    }

    @Override
    public void removeObjectInstance(int theObject, byte[] userSuppliedTag) {
        removeObjectInstance(theObject, userSuppliedTag, null, null);
    }

    @Override
    public void removeObjectInstance(int theObject, byte[] userSuppliedTag, LogicalTime theTime,
            EventRetractionHandle retractionHandle) {
        logger.trace("Object Removed: handle=" + theObject);
        objectInstanceRemovedListeners.forEach(listener -> {
            listener.objectInstanceRemoved(theObject, userSuppliedTag, theTime, retractionHandle);
        });
    }

    /**
     * The RTI has informed us that time regulation is now enabled.
     */
    @Override
    public void timeRegulationEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTime(theFederateTime);
        this.isRegulating = true;
    }

    @Override
    public void timeConstrainedEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTime(theFederateTime);
        this.isConstrained = true;
    }

    @Override
    public void timeAdvanceGrant(LogicalTime theTime) {
        this.federateTime = convertTime(theTime);
        this.isAdvancing = false;
    }

    public double getFederateTime() {
        return federateTime;
    }

    public double getFederateLookahead() {
        return federateLookahead;
    }

    public boolean isRegulating() {
        return isRegulating;
    }

    public boolean isConstrained() {
        return isConstrained;
    }

    public boolean isAdvancing() {
        return isAdvancing;
    }

    public boolean isAnnounced() {
        return isAnnounced;
    }

    public boolean isReadyToRun() {
        return isReadyToRun;
    }

    public void setFederateTime(double federateTime) {
        this.federateTime = federateTime;
    }

    public void setFederateLookahead(double federateLookahead) {
        this.federateLookahead = federateLookahead;
    }

    public void setRegulating(boolean isRegulating) {
        this.isRegulating = isRegulating;
    }

    public void setConstrained(boolean isConstrained) {
        this.isConstrained = isConstrained;
    }

    public void setAdvancing(boolean isAdvancing) {
        this.isAdvancing = isAdvancing;
    }

    public void setAnnounced(boolean isAnnounced) {
        this.isAnnounced = isAnnounced;
    }

    public void setReadyToRun(boolean isReadyToRun) {
        this.isReadyToRun = isReadyToRun;
    }

    @Override
    public void discoverObjectInstance(int theObject, int theObjectClass, String objectName) {
        logger.trace("New object of class {} created ({}:'{}')", theObjectClass, theObject, objectName);
        objectInstanceCreatedListeners.forEach(listener -> {
            listener.notifyObjectInstanceDiscovery(theObject, theObjectClass, objectName);
        });
    }

    @Override
    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes, byte[] tag) {
        reflectAttributeValues(theObject, theAttributes, tag, null, null);
    }

    @Override
    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes, byte[] userSuppliedTag,
            LogicalTime theTime, EventRetractionHandle retractionHandle) {
        logger.trace("Attributes of object {} changed", theObject);
        attributesUpdatedListeners.forEach(listener -> {
            Double time = null;
            if (theTime != null) {
                time = convertTime(theTime);
            }
            listener.notifyAttributesUpdated(theObject, theAttributes, userSuppliedTag, time, retractionHandle);
        });
    }

    public void registerObjectInstanceCreatedListener(ObjectInstanceCreatedListener listener) {
        objectInstanceCreatedListeners.add(listener);
    }

    public void registerAttributesUpdatedListener(AttributesUpdatedListener listener) {
        attributesUpdatedListeners.add(listener);
    }

    public void registerInteractionReceivedListener(InteractionReceivedListener listener) {
        interactionReceivedListeners.add(listener);
    }

    public void registerObjectInstanceRemovedListener(ObjectInstanceRemovedListener listener) {
        objectInstanceRemovedListeners.add(listener);
    }
}
