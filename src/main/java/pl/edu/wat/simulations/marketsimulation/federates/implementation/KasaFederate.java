package pl.edu.wat.simulations.marketsimulation.federates.implementation;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.ambassadors.implementation.KasaFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.federates.AbstractFederate;
import pl.edu.wat.simulations.marketsimulation.federates.Federate;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomInteraction;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomObject;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;
import pl.edu.wat.simulations.marketsimulation.objects.Checkout;
import pl.edu.wat.simulations.marketsimulation.objects.Customer;

public class KasaFederate extends AbstractFederate {
    public static final int MAX_SERVICE_TIME = 15;
    public static final int MIN_SERVICE_TIME = 10;
    public static final int MAX_QUEUE_SIZE = 5;
    public static final String ID_KASA = "idKasa";
    public static final String ID_KLIENT = "idKlient";
    public static final String PRZEPELNIONA = "przepelniona";
    public static final String DLUGOSC_KOLEJKI = "dlugoscKolejki";
    private static final String CZAS_OBSLUGI = "czasObslugi";
    private static final String CZAS_CZEKANIA_NA_OBSLUGE = "czasCzekaniaNaObsluge";
    private static final String UPRZYWILEJOWANY = "uprzywilejowany";
    public static final String ID = "id";
    private static final Logger logger = LoggerFactory.getLogger(KasaFederate.class);
    private FomObject kasaClassHandle;
    private FomInteraction otworzKaseClassHandle;
    private FomInteraction wejscieDoKolejkiClassHandle;
    private FomInteraction rozpoczecieObslugiClassHandle;
    private FomInteraction koniecObslugiClassHandle;
    private KasaFederateAmbassador federateAmbassador;
    private Map<Integer, Checkout> checkoutObjectIdsToObjects;
    private FomInteraction stopClassHandle;

    public KasaFederate(String federateName, String federationName, File fomFile)
            throws RTIinternalError, MalformedURLException {
        super(federateName, federationName, fomFile);
        checkoutObjectIdsToObjects = new HashMap<>();
    }

    @Override
    public void runFederate() throws RTIexception {
        while (isRunning) {
            executeAllQueuedTasks();
            logger.info("[{}] -- {}", getFederateAmbassador().getFederateTime(), this);
            checkoutObjectIdsToObjects.values().forEach(checkout -> {
                double federateTime = getFederateAmbassador().getFederateTime();
                checkout.updateCurrentBuyingCustomer(federateTime, (buyingCustomer, waitingTime) -> {
                    logger.info("[{}] {} started buying after waiting {}", federateTime, buyingCustomer, waitingTime);
                    sendBuyingStartedInteraction(waitingTime);
                    try {
                        updateCheckoutInRti(checkout.getObjectId(), checkout);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                });
                checkout.updateWithNewFederateTime(federateTime, finishedCustomer -> {
                    logger.info("[{}] {} finished buying after {} time units",
                            federateTime,
                            finishedCustomer,
                            finishedCustomer.getServiceTime());
                    sendBuyingFinishedInteraction(finishedCustomer.getServiceTime());
                });
            });
            advanceTime(1);
            logger.info("[{}] -- Time advanced to {}",
                    getFederateAmbassador().getFederateTime(),
                    getFederateAmbassador().getFederateTime());
        }
    }

    private void sendBuyingFinishedInteraction(double buyingTime) {
        logger.info("Sending buying time of {}", buyingTime);
        SuppliedParameters parameters;
        try {
            parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
            parameters.add(koniecObslugiClassHandle.getHandleFor(CZAS_OBSLUGI),
                    EncodingHelpers.encodeDouble(buyingTime));
            rtiAmbassador.sendInteraction(koniecObslugiClassHandle.getClassHandle(), parameters, generateTag());
        } catch (RTIexception e) {
            logger.error("Couldn't send buying started interaction, because: {}", e.getMessage());
        }
    }

    private void sendBuyingStartedInteraction(Double waitingTime) {
        logger.info("Sending waiting time of {}", waitingTime);
        SuppliedParameters parameters;
        try {
            parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
            parameters.add(rozpoczecieObslugiClassHandle.getHandleFor(CZAS_CZEKANIA_NA_OBSLUGE),
                    EncodingHelpers.encodeDouble(waitingTime));
            rtiAmbassador.sendInteraction(rozpoczecieObslugiClassHandle.getClassHandle(), parameters, generateTag());
        } catch (RTIexception e) {
            logger.error("Couldn't send buying started interaction, because: {}", e.getMessage());
        }
    }

    private static void executeFederate(String[] args) {
        String federateName = "kasaFederate";
        String federationName = AbstractFederateAmbassador.FEDERATION_NAME;
        String pathname = AbstractFederate.FOM_PATH;
        if (args.length > 0) {
            federateName = args[0];
        }
        if (args.length > 1) {
            federationName = args[1];
        }
        if (args.length > 2) {
            pathname = args[2];
        }
        try {
            logger.info("Creating federate");
            Federate federate = new KasaFederate(federateName, federationName, new File(pathname));
            federate.initiateFederate();
            federate.runFederate();
            federate.cleanUpFederate();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RTIexception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected AbstractFederateAmbassador getFederateAmbassador() {
        return federateAmbassador;
    }

    @Override
    protected void prepareFederationAmbassador() {
        federateAmbassador = new KasaFederateAmbassador();
        federateAmbassador.registerInteractionReceivedListener(
                (interactionClass, theInteraction, tag, theTime, eventRetractionHandle) -> {
                    if (interactionClass == otworzKaseClassHandle.getClassHandle()) {
                        submitNewTask(() -> {
                            logger.info("Opening new checkout");
                            initiateNewCheckout();
                        });
                    } else if (interactionClass == wejscieDoKolejkiClassHandle.getClassHandle()) {
                        submitNewTask(() -> {
                            prepareCustomerAndUpdateCheckoutInRti(theInteraction);
                        });
                    } else if (interactionClass == stopClassHandle.getClassHandle()) {
                        logger.info("Stop interaction received");
                        stop();
                    }
                });
    }

    private void prepareCustomerAndUpdateCheckoutInRti(ReceivedInteraction theInteraction) {
        logger.info("Received wejscieDoKolejki");
        try {
            Customer customer = new Customer(getFederateAmbassador().getFederateTime(), null,
                    (double) ThreadLocalRandom.current().nextInt(MAX_SERVICE_TIME - MIN_SERVICE_TIME + 1)
                            + MIN_SERVICE_TIME);
            Pair<Integer, Integer> checkoutAndCustomerId = getCheckoutAndCustomerIdParameters(theInteraction, customer);
            Checkout checkout = checkoutObjectIdsToObjects.get(checkoutAndCustomerId.getA());
            checkout.addCustomer(customer);
            logger.info("Customer {} entered queue in checkout {}",
                    checkoutAndCustomerId.getB(),
                    checkoutAndCustomerId.getA());
            logger.info("{}", customer);
            updateCheckoutInRti(checkoutAndCustomerId.getA(), checkout);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private Pair<Integer, Integer> getCheckoutAndCustomerIdParameters(ReceivedInteraction theInteraction,
            Customer customer) throws ArrayIndexOutOfBounds {
        int checkoutId = -1;
        int customerId = -1;
        for (int i = 0; i < theInteraction.size(); i++) {
            int attributeHandle = theInteraction.getParameterHandle(i);
            String nameFor = wejscieDoKolejkiClassHandle.getNameFor(attributeHandle);
            byte[] value = theInteraction.getValue(i);
            if (nameFor.equalsIgnoreCase(ID_KASA)) {
                checkoutId = EncodingHelpers.decodeInt(value);
            } else if (nameFor.equalsIgnoreCase(ID_KLIENT)) {
                customerId = EncodingHelpers.decodeInt(value);
            } else if (nameFor.equalsIgnoreCase(UPRZYWILEJOWANY)) {
                customer.setPriviledged(EncodingHelpers.decodeBoolean(value));
            }
        }
        return new Pair<>(checkoutId, customerId);
    }

    private void updateCheckoutInRti(int checkoutId, Checkout checkout)
            throws RTIinternalError, ObjectNotKnown, AttributeNotDefined, AttributeNotOwned, FederateNotExecutionMember,
            SaveInProgress, RestoreInProgress, ConcurrentAccessAttempted {
        SuppliedAttributes attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();
        byte[] encodedQueueSize = EncodingHelpers.encodeInt(checkout.getQueueSize());
        byte[] encodedIsFilled = EncodingHelpers.encodeBoolean(checkout.isFilled());
        attributes.add(kasaClassHandle.getHandleFor(DLUGOSC_KOLEJKI), encodedQueueSize);
        attributes.add(kasaClassHandle.getHandleFor(PRZEPELNIONA), encodedIsFilled);
        try {
            rtiAmbassador.updateAttributeValues(checkoutId,
                    attributes,
                    generateTag(),
                    convertTime(federateAmbassador.getFederateTime() + 1));
        } catch (InvalidFederationTime e) {
            logger.error("Couldn't update Checkout {}, because of invalid federation time error: {}",
                    checkoutId,
                    e.getMessage());
        }
    }

    @Override
    protected void initiateObjects() throws RTIexception {
        initiateNewCheckout();
    }

    private void initiateNewCheckout() {
        try {
            int checkoutObjectId = rtiAmbassador.registerObjectInstance(kasaClassHandle.getClassHandle());
            Checkout checkout = new Checkout(checkoutObjectId, MAX_QUEUE_SIZE);
            checkoutObjectIdsToObjects.put(checkoutObjectId, checkout);
            updateCheckoutInRti(checkoutObjectId, checkout);
        } catch (Exception e) {
            logger.error("Couldn't open new checkout, because: {}", e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishAndSubscribe() throws RTIexception {
        kasaClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Kasa"),
                new Pair<String, Class<?>>(ID, Integer.class),
                new Pair<String, Class<?>>(DLUGOSC_KOLEJKI, Integer.class),
                new Pair<String, Class<?>>(PRZEPELNIONA, Boolean.class));
        rtiAmbassador.publishObjectClass(kasaClassHandle.getClassHandle(), kasaClassHandle.createAttributeHandleSet());

        otworzKaseClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.OtworzKase"));
        rtiAmbassador.subscribeInteractionClass(otworzKaseClassHandle.getClassHandle());

        wejscieDoKolejkiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.WejscieDoKolejki"),
                new Pair<String, Class<?>>(ID_KLIENT, Integer.class),
                new Pair<String, Class<?>>(ID_KASA, Integer.class),
                new Pair<String, Class<?>>(UPRZYWILEJOWANY, Boolean.class));
        rtiAmbassador.subscribeInteractionClass(wejscieDoKolejkiClassHandle.getClassHandle());

        rozpoczecieObslugiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.RozpoczecieObslugi"),
                new Pair<String, Class<?>>(CZAS_CZEKANIA_NA_OBSLUGE, Float.class));
        rtiAmbassador.publishInteractionClass(rozpoczecieObslugiClassHandle.getClassHandle());

        koniecObslugiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.KoniecObslugi"),
                new Pair<String, Class<?>>(CZAS_OBSLUGI, Float.class));
        rtiAmbassador.publishInteractionClass(koniecObslugiClassHandle.getClassHandle());

        stopClassHandle = prepareFomInteraction(rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.Stop"));
        rtiAmbassador.subscribeInteractionClass(stopClassHandle.getClassHandle());
    }

    @Override
    protected void deleteObjects() throws RTIexception {
        checkoutObjectIdsToObjects.forEach((checkoutObjectId, checkout) -> {
            logger.info("Removing object {}:{}", checkoutObjectId, checkout);
            try {
                rtiAmbassador.deleteObjectInstance(checkoutObjectId, generateTag());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
    }

    public static void main(String[] args) {
        executeFederate(args);
    }

    @Override
    public String toString() {
        return "checkouts[" + checkoutObjectIdsToObjects.size() + "] = " + checkoutObjectIdsToObjects + "]";
    }
}
