package pl.edu.wat.simulations.marketsimulation.helpers;

@FunctionalInterface
public interface GenerationStrategy<T> {
    public boolean shouldGenerate(T update);
}
