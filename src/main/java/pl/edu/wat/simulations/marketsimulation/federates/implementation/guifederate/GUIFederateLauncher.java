package pl.edu.wat.simulations.marketsimulation.federates.implementation.guifederate;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.federates.AbstractFederate;

public class GUIFederateLauncher extends Application {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private FXMLLoader fxmlLoader;
    private GUIFederateController controller;
    private GUIFederate federate;

    private ExecutorService federateExecutor;

    @Override
    public void start(Stage primaryStage) throws Exception {
        federateExecutor = Executors.newFixedThreadPool(1);
        fxmlLoader = new FXMLLoader();
        Scene scene = new Scene(fxmlLoader.load(getClass().getResource("/view/ui.fxml").openStream()));
        controller = fxmlLoader.getController();
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.sizeToScene();
        primaryStage.centerOnScreen();
        primaryStage.setResizable(false);
        controller.changeMainButtonText("Start federate");
        controller.registerStopButtonAction(() -> {
            if (federate != null) {
                federate.sendStopInteraction();
            }
            controller.disableStopButton();
            Platform.exit();
        });
        controller.registerMainButtonAction(() -> {
            controller.disableMainButton();
            controller.changeMainButtonText("Achieve sync point");
            controller.registerMainButtonAction(() -> {
                controller.disableMainButton();
                logger.info("Proceeding");
                federate.proceed();
                controller.changeMainButtonText("Close the market");
                controller.registerMainButtonAction(() -> {
                    federate.scheduleCloseTheMarketInteraction();
                });
            });
            federateExecutor.submit(() -> {
                List<String> args = getParameters().getRaw();
                String federateName = "guiFederate";
                String federationName = AbstractFederateAmbassador.FEDERATION_NAME;
                String pathname = AbstractFederate.FOM_PATH;
                if (args.size() > 0) {
                    federateName = args.get(0);
                }
                if (args.size() > 1) {
                    federationName = args.get(1);
                }
                if (args.size() > 2) {
                    pathname = args.get(2);
                }
                try {
                    logger.info("Creating new GUIFederate with name {} in federation {} with fom file {}",
                            federateName,
                            federationName,
                            pathname);
                    controller.setFederationStatus("Creating " + federateName + " federate");
                    federate = new GUIFederate(federateName, federationName, new File(pathname), controller);
                    controller.enableMainButton();
                    controller.setFederationStatus("Initiating " + federateName + " federate");
                    federate.initiateFederate();
                    controller.enableMainButton();
                    controller.setFederationStatus("Running " + federateName + " federate");
                    federate.runFederate();
                    controller.disableMainButton();
                    federate.cleanUpFederate();
                    federate = null;
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            });
        });
    }

    @Override
    public void stop() throws Exception {
        logger.info("Awaiting termination");
        controller.setFederationStatus("Awaiting termination");
        if (federate != null) {
            federate.stop();
        }
        federateExecutor.shutdown();
        federateExecutor.awaitTermination(10, TimeUnit.SECONDS);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
