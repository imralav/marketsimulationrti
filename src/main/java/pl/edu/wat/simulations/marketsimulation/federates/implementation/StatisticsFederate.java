package pl.edu.wat.simulations.marketsimulation.federates.implementation;

import java.io.File;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.ambassadors.implementation.StatystykaFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.federates.AbstractFederate;
import pl.edu.wat.simulations.marketsimulation.federates.Federate;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomInteraction;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomObject;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;

public class StatisticsFederate extends AbstractFederate {
    private static final Logger logger = LoggerFactory.getLogger(StatisticsFederate.class);
    private static final String CZAS_OBSLUGI = "czasObslugi";
    private static final String CZAS_CZEKANIA_NA_OBSLUGE = "czasCzekaniaNaObsluge";
    private static final String ID = "id";
    private static final String NR_KASY = "nrKasy";
    private static final String NR_W_KOLEJCE = "nrWKolejce";
    private static final String UPRZYWILEJOWANY = "uprzywilejowany";
    private static final String CZAS_ZAKUPOW = "czasZakupow";

    private FomObject klientClassHandle;
    private FomInteraction rozpoczecieObslugiClassHandle;
    private FomInteraction koniecObslugiClassHandle;

    private StatystykaFederateAmbassador fedAmbassador;
    private Collection<Double> buyingTimes;
    private Collection<Double> queueWaitingTimes;
    private Collection<Double> shoppingTimes;
    private Map<Integer, Integer> customerObjectIdToClassHandlesMap;
    private FomObject statisticsClassHandle;
    private int statisticsObjectHandle;
    private FomInteraction stopClassHandle;

    public StatisticsFederate(String federateName, String federationName, File fomFile)
            throws RTIinternalError, MalformedURLException {
        super(federateName, federationName, fomFile);
        buyingTimes = new ArrayList<>();
        queueWaitingTimes = new ArrayList<>();
        shoppingTimes = new ArrayList<>();
        customerObjectIdToClassHandlesMap = new HashMap<>();
    }

    public static void main(String[] args) {
        executeFederate(args);
    }

    @Override
    public void runFederate() throws RTIexception {
        while (isRunning) {
            double averageBuyingTime = getAverage(buyingTimes);
            double averageQueueWaitingTime = getAverage(queueWaitingTimes);
            double averageShoppingTimes = getAverage(shoppingTimes);
            sendCurrentStatistics(averageBuyingTime, averageQueueWaitingTime, averageShoppingTimes);
            advanceTime(2);
        }
    }

    private void sendCurrentStatistics(double averageServiceTime, double averageWaitingTime,
            double averageShoppingTime) {
        logger.info("Current statistics: averageServiceTime: {} averageWaitingTime: {} averageShoppingTimes: {}",
                new DecimalFormat("#.##").format(averageServiceTime),
                new DecimalFormat("#.##").format(averageWaitingTime),
                new DecimalFormat("#.##").format(averageShoppingTime));
        try {
            SuppliedAttributes attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();
            attributes.add(statisticsClassHandle.getHandleFor("avgShoppingTime"),
                    EncodingHelpers.encodeDouble(averageShoppingTime));
            attributes.add(statisticsClassHandle.getHandleFor("avgWaitingTime"),
                    EncodingHelpers.encodeDouble(averageWaitingTime));
            attributes.add(statisticsClassHandle.getHandleFor("avgServiceTime"),
                    EncodingHelpers.encodeDouble(averageServiceTime));
            rtiAmbassador.updateAttributeValues(statisticsObjectHandle, attributes, generateTag());
        } catch (RTIexception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private double getAverage(Collection<Double> buyingTimes2) {
        return buyingTimes2.stream().mapToDouble(value -> {
            return value;
        }).average().orElseGet(() -> {
            return 0;
        });
    }

    @Override
    protected AbstractFederateAmbassador getFederateAmbassador() {
        return fedAmbassador;
    }

    @Override
    protected void prepareFederationAmbassador() {
        fedAmbassador = new StatystykaFederateAmbassador();
        fedAmbassador.registerObjectInstanceCreatedListener((int theObject, int theObjectClass, String objectName) -> {
            if (theObjectClass == klientClassHandle.getClassHandle()) {
                logger.info("New customer entered. Object handle: {}, class handle: {}", theObject, theObjectClass);
                customerObjectIdToClassHandlesMap.put(theObject, theObjectClass);
            }
        });
        fedAmbassador.registerAttributesUpdatedListener((theObject, theAttributes, tag, theTime, retractionHandle) -> {
            if (customerObjectIdToClassHandlesMap.get(theObject).equals(klientClassHandle.getClassHandle())) {
                updateShoppingTimes(theAttributes);
            }
        });
        fedAmbassador.registerInteractionReceivedListener((int interactionClass, ReceivedInteraction theInteraction,
                byte[] tag, LogicalTime theTime, EventRetractionHandle eventRetractionHandle) -> {
            if (interactionClass == rozpoczecieObslugiClassHandle.getClassHandle()) {
                updateQueueWaitingTimes(theInteraction);
            } else if (interactionClass == koniecObslugiClassHandle.getClassHandle()) {
                updateQueueBuyingTimes(theInteraction);
            } else if (interactionClass == stopClassHandle.getClassHandle()) {
                logger.info("Stop interaction received");
                stop();
            }
        });
    }

    private void updateShoppingTimes(ReflectedAttributes theAttributes) {
        double shoppingTime = extractShoppingTime(theAttributes);
        logger.info("Adding shopping time of {}", shoppingTime);
        shoppingTimes.add(shoppingTime);
    }

    private double extractShoppingTime(ReflectedAttributes theAttributes) {
        double shoppingTime = -1;
        for (int i = 0; i < theAttributes.size(); i++) {
            try {
                byte[] value = theAttributes.getValue(i);
                if (theAttributes.getAttributeHandle(i) == klientClassHandle.getHandleFor(CZAS_ZAKUPOW)) {
                    shoppingTime = EncodingHelpers.decodeDouble(value);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return shoppingTime;
    }

    private void updateQueueBuyingTimes(ReceivedInteraction theInteraction) {
        double extractBuyingTime = extractBuyingTime(theInteraction);
        logger.info("Adding buying time of {}", extractBuyingTime);
        buyingTimes.add(extractBuyingTime);
    }

    private void updateQueueWaitingTimes(ReceivedInteraction theInteraction) {
        double extractWaitingTime = extractWaitingTime(theInteraction);
        logger.info("Adding waiting time of {}", extractWaitingTime);
        queueWaitingTimes.add(extractWaitingTime);
    }

    private double extractWaitingTime(ReceivedInteraction theInteraction) {
        double waitingTime = -1;
        for (int i = 0; i < theInteraction.size(); i++) {
            try {
                int attributeHandle = theInteraction.getParameterHandle(i);
                String nameFor = rozpoczecieObslugiClassHandle.getNameFor(attributeHandle);
                if (nameFor.equalsIgnoreCase(CZAS_CZEKANIA_NA_OBSLUGE)) {
                    waitingTime = EncodingHelpers.decodeDouble(theInteraction.getValue(i));
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return waitingTime;
    }

    private double extractBuyingTime(ReceivedInteraction theInteraction) {
        double buyingTime = -1;
        for (int i = 0; i < theInteraction.size(); i++) {
            try {
                int attributeHandle = theInteraction.getParameterHandle(i);
                String nameFor = koniecObslugiClassHandle.getNameFor(attributeHandle);
                if (nameFor.equalsIgnoreCase(CZAS_OBSLUGI)) {
                    buyingTime = EncodingHelpers.decodeDouble(theInteraction.getValue(i));
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return buyingTime;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishAndSubscribe() throws RTIexception {
        klientClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Klient"),
                new Pair<String, Class<?>>(ID, Integer.class),
                new Pair<String, Class<?>>(CZAS_ZAKUPOW, Integer.class),
                new Pair<String, Class<?>>(UPRZYWILEJOWANY, Boolean.class),
                new Pair<String, Class<?>>(NR_W_KOLEJCE, Integer.class),
                new Pair<String, Class<?>>(NR_KASY, Integer.class));
        rtiAmbassador.subscribeObjectClassAttributes(klientClassHandle.getClassHandle(),
                klientClassHandle.createAttributeHandleSet());

        statisticsClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Statistics"),
                new Pair<String, Class<?>>("avgShoppingTime", Double.class),
                new Pair<String, Class<?>>("avgWaitingTime", Double.class),
                new Pair<String, Class<?>>("avgServiceTime", Double.class));
        rtiAmbassador.publishObjectClass(statisticsClassHandle.getClassHandle(),
                statisticsClassHandle.createAttributeHandleSet());

        rozpoczecieObslugiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.RozpoczecieObslugi"),
                new Pair<String, Class<?>>(CZAS_CZEKANIA_NA_OBSLUGE, Integer.class));
        rtiAmbassador.subscribeInteractionClass(rozpoczecieObslugiClassHandle.getClassHandle());

        koniecObslugiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.KoniecObslugi"),
                new Pair<String, Class<?>>(CZAS_OBSLUGI, Integer.class));
        rtiAmbassador.subscribeInteractionClass(koniecObslugiClassHandle.getClassHandle());

        stopClassHandle = prepareFomInteraction(rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.Stop"));
        rtiAmbassador.subscribeInteractionClass(stopClassHandle.getClassHandle());
    }

    @Override
    protected void initiateObjects() throws RTIexception {
        statisticsObjectHandle = rtiAmbassador.registerObjectInstance(statisticsClassHandle.getClassHandle());
    }

    private static void executeFederate(String[] args) {
        String federateName = "statystykaFederate";
        String federationName = AbstractFederateAmbassador.FEDERATION_NAME;
        String pathname = AbstractFederate.FOM_PATH;
        if (args.length > 0) {
            federateName = args[0];
        }
        if (args.length > 1) {
            federationName = args[1];
        }
        if (args.length > 2) {
            pathname = args[2];
        }
        try {
            Federate federate = new StatisticsFederate(federateName, federationName, new File(pathname));
            federate.initiateFederate();
            federate.runFederate();
            federate.cleanUpFederate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
