package pl.edu.wat.simulations.marketsimulation.listeners;

@FunctionalInterface
public interface ObjectInstanceCreatedListener {
    public void notifyObjectInstanceDiscovery(int theObject, int theObjectClass, String objectName);
}
