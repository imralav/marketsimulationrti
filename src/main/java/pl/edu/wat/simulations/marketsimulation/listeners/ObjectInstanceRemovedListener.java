package pl.edu.wat.simulations.marketsimulation.listeners;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;

@FunctionalInterface
public interface ObjectInstanceRemovedListener {
    public void objectInstanceRemoved(int theObject, byte[] userSuppliedTag, LogicalTime theTime,
            EventRetractionHandle retractionHandle);
}
