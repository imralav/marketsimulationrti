package pl.edu.wat.simulations.marketsimulation.objects;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Checkout {
    private int objectId;
    private int maxQueueSize;
    private List<Customer> customersQueue;
    private Customer buyingCustomer;

    public Checkout(int objectId, int maxQueueSize) {
        this.objectId = objectId;
        this.maxQueueSize = maxQueueSize;
        customersQueue = new LinkedList<>();
    }

    public int getQueueSize() {
        return customersQueue.size();
    }

    public boolean isFilled() {
        return getQueueSize() >= maxQueueSize;
    }

    public void addCustomer(Customer customer) {
        if (customer.isPriviledged()) {
            customersQueue.add(0, customer);
        } else {
            customersQueue.add(customer);
        }
    }

    public Customer getCurrentlyBuyingCustomer() {
        return buyingCustomer;
    }

    public int getObjectId() {
        return objectId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + objectId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Checkout other = (Checkout) obj;
        if (objectId != other.objectId)
            return false;
        return true;
    }

    public void updateCurrentBuyingCustomer(double federateTime,
            BiConsumer<Customer, Double> customerStartedBuyingAction) {
        if (buyingCustomer == null && customersQueue.size() > 0) {
            buyingCustomer = customersQueue.remove(0);
            double queueWaitingTime = federateTime - buyingCustomer.getOldFederateTime();
            buyingCustomer.setOldFederateTime(federateTime);
            customerStartedBuyingAction.accept(buyingCustomer, queueWaitingTime);
        }
    }

    public void updateWithNewFederateTime(double federateTime, Consumer<Customer> customerFinishedBuyingAction) {
        if (buyingCustomer != null) {
            buyingCustomer.updateWithNewFederateTime(federateTime);
            if (buyingCustomer.hasServiceFinished()) {
                customerFinishedBuyingAction.accept(buyingCustomer);
                buyingCustomer = null;
            }
        }
    }

    @Override
    public String toString() {
        return "Checkout [objectId=" + objectId + ", maxQueueSize=" + maxQueueSize + ", customersQueue.size["
                + customersQueue.size() + "], buyingCustomer=" + buyingCustomer + "]";
    }
}
