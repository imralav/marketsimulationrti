package pl.edu.wat.simulations.marketsimulation.helpers;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;

public class GenerationStrategyFactory {
    private static class RandomGenerationStrategy implements GenerationStrategy<Double> {
        private Double factor;

        RandomGenerationStrategy(Double factor) {
            this.factor = factor;
        }

        @Override
        public boolean shouldGenerate(Double update) {
            return ThreadLocalRandom.current().nextDouble() < factor;
        }
    }

    private static class CustomGenerationStrategy<T> implements GenerationStrategy<T> {
        private Function<T, Boolean> customAction;

        CustomGenerationStrategy(Function<T, Boolean> customAction) {
            this.customAction = customAction;
        }

        @Override
        public boolean shouldGenerate(T federationTime) {
            return customAction.apply(federationTime);
        }

    }

    private static class NopGenerationStrategy implements GenerationStrategy<Double> {

        @Override
        public boolean shouldGenerate(Double update) {
            return false;
        }
    }

    /**
     * creates generation strategy that creates customer with a chance provided in parameter
     * 
     * @param factor
     *            chance of creating new customer, between 0 (never) and 1 (always)
     * @return
     */
    public static GenerationStrategy<Double> randomGenerationStrategy(Double factor) {
        return new RandomGenerationStrategy(factor);
    }

    public static <T> GenerationStrategy<T> customGenerationStrategy(Function<T, Boolean> action) {
        return new CustomGenerationStrategy<T>(action);
    }

    public static GenerationStrategy<Double> nopGenerationStrategy() {
        return new NopGenerationStrategy();
    }
}
