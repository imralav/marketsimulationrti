package pl.edu.wat.simulations.marketsimulation.federates.implementation.guifederate;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.ambassadors.implementation.GUIFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.federates.AbstractFederate;
import pl.edu.wat.simulations.marketsimulation.fomstructure.AbstractFomEntity;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomInteraction;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomObject;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;

public class GUIFederate extends AbstractFederate {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private static final String CZAS_OBSLUGI = "czasObslugi";
    private static final String CZAS_CZEKANIA_NA_OBSLUGE = "czasCzekaniaNaObsluge";
    private static final String ID = "id";
    private static final String ID_KASA = "idKasa";
    private static final String ID_KLIENT = "idKlient";
    private static final String PRZEPELNIONA = "przepelniona";
    private static final String DLUGOSC_KOLEJKI = "dlugoscKolejki";
    private static final String NR_KASY = "nrKasy";
    private static final String NR_W_KOLEJCE = "nrWKolejce";
    private static final String UPRZYWILEJOWANY = "uprzywilejowany";
    private static final String CZAS_ZAKUPOW = "czasZakupow";
    private FomObject kasaClassHandle;
    private FomObject klientClassHandle;
    private FomInteraction rozpoczecieObslugiClassHandle;
    private FomInteraction koniecObslugiClassHandle;
    private FomInteraction wejscieDoKolejkiClassHandle;
    private FomInteraction otworzKaseClassHandle;
    private boolean shouldProceed = false;
    private GUIFederateAmbassador fedAmbassador;
    private GUIFederateController controller;
    private Map<Integer, Integer> checkoutObjectHandleToClassHandleMap;
    private Map<Integer, Integer> customerObjectHandleToClassHandleMap;
    private Map<Integer, Pair<Integer, Boolean>> checkoutObjectHandleToQueueSizeAndFilledMap;
    private List<Integer> shoppingCustomers;
    private FomObject statisticsClassHandle;
    private Integer statisticsObjectHandle;
    private int customersLeft = 0;
    private boolean shouldSendCloseTheMarketInteraction = false;
    private FomInteraction closeTheMarketClassHandle;
    private boolean shouldSendStopInteraction = false;
    private AbstractFomEntity stopClassHandle;

    public GUIFederate(String federateName, String federationName, File fomFile, GUIFederateController controller)
            throws RTIinternalError, MalformedURLException {
        super(federateName, federationName, fomFile);
        this.controller = controller;
        checkoutObjectHandleToClassHandleMap = new HashMap<>();
        customerObjectHandleToClassHandleMap = new HashMap<>();
        checkoutObjectHandleToQueueSizeAndFilledMap = new HashMap<>();
        shoppingCustomers = new LinkedList<>();
    }

    @Override
    public void runFederate() throws RTIexception {
        while (isRunning) {
            controller.setFederationStatus(
                    "Federation " + federationName + " running [" + getFederateAmbassador().getFederateTime() + "]");
            controller.setShoppingCustomers(shoppingCustomers.size());
            controller.setCustomersLeft(customersLeft);
            optionallySendCloseTheMarketInteraction();
            advanceTime(1);
        }
        optionallySendStopFederationInteraction();
    }

    private void optionallySendStopFederationInteraction() {
        if (shouldSendStopInteraction) {
            logger.info("Sending \"stop\" interaction");
            SuppliedParameters parameters;
            try {
                parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
                rtiAmbassador.sendInteraction(stopClassHandle.getClassHandle(), parameters, generateTag());
            } catch (RTIexception e) {
                logger.error("Couldn't send \"stop\" interaction, because: {}", e.getMessage());
            }
            shouldSendStopInteraction = false;
        }
    }

    private void optionallySendCloseTheMarketInteraction() {
        if (shouldSendCloseTheMarketInteraction) {
            logger.info("Sending \"close market\" interaction");
            SuppliedParameters parameters;
            try {
                parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
                rtiAmbassador.sendInteraction(closeTheMarketClassHandle.getClassHandle(), parameters, generateTag());
            } catch (RTIexception e) {
                logger.error("Couldn't send \"close the market\" interaction, because: {}", e.getMessage());
            }
            shouldSendCloseTheMarketInteraction = false;
        }
    }

    @Override
    public void cleanUpFederate() throws RTIexception {
        controller.setFederationStatus("Cleaning up the federate");
        super.cleanUpFederate();
    }

    @Override
    protected AbstractFederateAmbassador getFederateAmbassador() {
        return fedAmbassador;
    }

    @Override
    protected void prepareFederationAmbassador() {
        fedAmbassador = new GUIFederateAmbassador();
        fedAmbassador.registerObjectInstanceCreatedListener((int theObject, int theObjectClass, String objectName) -> {
            if (theObjectClass == klientClassHandle.getClassHandle()) {
                customerObjectHandleToClassHandleMap.put(theObject, theObjectClass);
                shoppingCustomers.add(theObject);
                logger.info("Customer ({}) entered, customers amount: {}", theObject, shoppingCustomers.size());
            } else if (theObjectClass == kasaClassHandle.getClassHandle()) {
                logger.info("New checkout opened ({})", theObject);
                checkoutObjectHandleToClassHandleMap.put(theObject, theObjectClass);
            } else if (theObjectClass == statisticsClassHandle.getClassHandle()) {
                statisticsObjectHandle = theObject;
            }
        });
        fedAmbassador.registerObjectInstanceRemovedListener((int theObject, byte[] userSuppliedTag, LogicalTime theTime,
                EventRetractionHandle retractionHandle) -> {
            if (customerObjectHandleToClassHandleMap.get(theObject) == klientClassHandle.getClassHandle()) {
                shoppingCustomers.remove(new Integer(theObject));
                logger.info("Customer ({}) removed", theObject);
            }
        });
        fedAmbassador.registerInteractionReceivedListener((int interactionClass, ReceivedInteraction theInteraction,
                byte[] tag, LogicalTime theTime, EventRetractionHandle eventRetractionHandle) -> {
            if (interactionClass == wejscieDoKolejkiClassHandle.getClassHandle()) {
                int extractCustomerClassHandle = extractCustomerClassHandle(theInteraction);
                logger.info("Customer({}) entered queue", extractCustomerClassHandle);
                shoppingCustomers.remove(new Integer(extractCustomerClassHandle));
            } else if (interactionClass == koniecObslugiClassHandle.getClassHandle()) {
                controller.setCustomersLeft(++customersLeft);
            }
        });
        fedAmbassador.registerAttributesUpdatedListener((theObject, theAttributes, tag, theTime, whateverMan) -> {
            if (checkoutObjectHandleToClassHandleMap.containsKey(theObject)
                    && kasaClassHandle.getClassHandle() == checkoutObjectHandleToClassHandleMap.get(theObject)) {
                handleCheckoutUpdate(theObject, theAttributes);
            } else if (statisticsObjectHandle != null && statisticsObjectHandle == theObject) {
                extractAndUpdateStatistics(theAttributes);
            }
        });
    }

    private void extractAndUpdateStatistics(ReflectedAttributes theAttributes) {
        double avgShoppingTime = -1;
        double avgWaitingTime = -1;
        double avgServiceTime = -1;
        for (int i = 0; i < theAttributes.size(); i++) {
            try {
                byte[] value = theAttributes.getValue(i);
                if (theAttributes.getAttributeHandle(i) == statisticsClassHandle.getHandleFor("avgShoppingTime")) {
                    avgShoppingTime = EncodingHelpers.decodeDouble(value);
                } else
                    if (theAttributes.getAttributeHandle(i) == statisticsClassHandle.getHandleFor("avgWaitingTime")) {
                    avgWaitingTime = EncodingHelpers.decodeDouble(value);
                } else if (theAttributes.getAttributeHandle(i) == statisticsClassHandle
                        .getHandleFor("avgServiceTime")) {
                    avgServiceTime = EncodingHelpers.decodeDouble(value);
                }
            } catch (ArrayIndexOutOfBounds e) {
                logger.error(e.getMessage(), e);
            }
        }
        updateStatistics(avgShoppingTime, avgWaitingTime, avgServiceTime);
    }

    private void updateStatistics(double avgShoppingTime, double avgWaitingTime, double avgServiceTime) {
        controller.setAvgShoppingTime(avgShoppingTime);
        controller.setAvgWaitingTime(avgWaitingTime);
        controller.setAvgServiceTime(avgServiceTime);
    }

    private void handleCheckoutUpdate(int theObject, ReflectedAttributes theAttributes) {
        int queueSize = -1;
        boolean filled = false;
        for (int i = 0; i < theAttributes.size(); i++) {
            try {
                byte[] value = theAttributes.getValue(i);
                if (theAttributes.getAttributeHandle(i) == kasaClassHandle.getHandleFor(DLUGOSC_KOLEJKI)) {
                    queueSize = EncodingHelpers.decodeInt(value);
                } else if (theAttributes.getAttributeHandle(i) == kasaClassHandle.getHandleFor(PRZEPELNIONA)) {
                    filled = EncodingHelpers.decodeBoolean(value);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        logger.info("Checkout ({}) updated: queue size: {}, filled: {}", theObject, queueSize, filled);
        Pair<Integer, Boolean> value = new Pair<>(queueSize, filled);
        checkoutObjectHandleToQueueSizeAndFilledMap.put(theObject, value);
        controller.updateCheckouts(theObject, value);
    }

    private int extractCustomerClassHandle(ReceivedInteraction theInteraction) {
        int handle = -1;
        for (int i = 0; i < theInteraction.size(); i++) {
            try {
                if (theInteraction.getParameterHandle(i) == wejscieDoKolejkiClassHandle.getHandleFor(ID_KLIENT)) {
                    handle = EncodingHelpers.decodeInt(theInteraction.getValue(i));
                }
            } catch (ArrayIndexOutOfBounds e) {
                logger.error(e.getMessage(), e);
            }
        }
        return handle;
    }

    @Override
    protected void waitForUser() {
        logger.info("Waiting until proceed action received from gui");
        controller.setFederationStatus("Waiting for synch point");
        while (!shouldProceed && isRunning) {
            logger.trace("Still waiting...");
        }
        logger.info("Proceed action received from gui, continuing");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishAndSubscribe() throws RTIexception {
        kasaClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Kasa"),
                new Pair<String, Class<?>>(ID, Integer.class),
                new Pair<String, Class<?>>(DLUGOSC_KOLEJKI, Integer.class),
                new Pair<String, Class<?>>(PRZEPELNIONA, Boolean.class));
        rtiAmbassador.subscribeObjectClassAttributes(kasaClassHandle.getClassHandle(),
                kasaClassHandle.createAttributeHandleSet());

        klientClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Klient"),
                new Pair<String, Class<?>>(ID, Integer.class),
                new Pair<String, Class<?>>(CZAS_ZAKUPOW, Integer.class),
                new Pair<String, Class<?>>(UPRZYWILEJOWANY, Boolean.class),
                new Pair<String, Class<?>>(NR_W_KOLEJCE, Integer.class),
                new Pair<String, Class<?>>(NR_KASY, Integer.class));
        rtiAmbassador.subscribeObjectClassAttributes(klientClassHandle.getClassHandle(),
                klientClassHandle.createAttributeHandleSet());

        statisticsClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Statistics"),
                new Pair<String, Class<?>>("avgShoppingTime", Double.class),
                new Pair<String, Class<?>>("avgWaitingTime", Double.class),
                new Pair<String, Class<?>>("avgServiceTime", Double.class));
        rtiAmbassador.subscribeObjectClassAttributes(statisticsClassHandle.getClassHandle(),
                statisticsClassHandle.createAttributeHandleSet());

        rozpoczecieObslugiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.RozpoczecieObslugi"),
                new Pair<String, Class<?>>(CZAS_CZEKANIA_NA_OBSLUGE, Integer.class));
        rtiAmbassador.subscribeInteractionClass(rozpoczecieObslugiClassHandle.getClassHandle());

        koniecObslugiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.KoniecObslugi"),
                new Pair<String, Class<?>>(CZAS_OBSLUGI, Integer.class));
        rtiAmbassador.subscribeInteractionClass(koniecObslugiClassHandle.getClassHandle());

        wejscieDoKolejkiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.WejscieDoKolejki"),
                new Pair<String, Class<?>>(ID_KLIENT, Integer.class),
                new Pair<String, Class<?>>(ID_KASA, Integer.class));
        rtiAmbassador.subscribeInteractionClass(wejscieDoKolejkiClassHandle.getClassHandle());

        otworzKaseClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.OtworzKase"));
        rtiAmbassador.subscribeInteractionClass(otworzKaseClassHandle.getClassHandle());

        closeTheMarketClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.CloseMarket"));
        rtiAmbassador.publishInteractionClass(closeTheMarketClassHandle.getClassHandle());

        stopClassHandle = prepareFomInteraction(rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.Stop"));
        rtiAmbassador.publishInteractionClass(stopClassHandle.getClassHandle());
    }

    public void proceed() {
        this.shouldProceed = true;
    }

    public boolean shouldProceed() {
        return shouldProceed;
    }

    public void scheduleCloseTheMarketInteraction() {
        shouldSendCloseTheMarketInteraction = true;
    }

    public void sendStopInteraction() {
        shouldSendStopInteraction = true;
    }

}
