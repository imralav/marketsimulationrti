package pl.edu.wat.simulations.marketsimulation.fomstructure;

public interface ClassHandleContainer {
    public int getClassHandle();
}
