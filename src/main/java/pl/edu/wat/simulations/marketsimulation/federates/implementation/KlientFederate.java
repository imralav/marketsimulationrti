package pl.edu.wat.simulations.marketsimulation.federates.implementation;

import java.io.File;
import java.net.MalformedURLException;
import java.time.Duration;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import pl.edu.wat.simulations.marketsimulation.ambassadors.AbstractFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.ambassadors.implementation.KlientFederateAmbassador;
import pl.edu.wat.simulations.marketsimulation.federates.AbstractFederate;
import pl.edu.wat.simulations.marketsimulation.federates.Federate;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomInteraction;
import pl.edu.wat.simulations.marketsimulation.fomstructure.FomObject;
import pl.edu.wat.simulations.marketsimulation.helpers.GenerationStrategy;
import pl.edu.wat.simulations.marketsimulation.helpers.GenerationStrategyFactory;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;
import pl.edu.wat.simulations.marketsimulation.helpers.StopWatch;
import pl.edu.wat.simulations.marketsimulation.objects.Customer;

public class KlientFederate extends AbstractFederate {
    private static final Duration LOOP_MINIMAL_DURATION = Duration.ofMillis(200);
    public static final int MAX_SHOPPING_TIME = 10;
    public static final int MIN_SHOPPING_TIME = 5;

    private static final double CUSTOMER_GENERATION_FACTOR = 0.8f;

    private static final Logger logger = LoggerFactory.getLogger(KlientFederate.class);

    private static final String NR_KASY = "nrKasy";
    private static final String NR_W_KOLEJCE = "nrWKolejce";
    private static final String UPRZYWILEJOWANY = "uprzywilejowany";
    private static final String CZAS_ZAKUPOW = "czasZakupow";
    private static final String ID = "id";
    private static final String ID_KLIENT = "idKlient";
    private static final String ID_KASA = "idKasa";
    private static final String DLUGOSC_KOLEJKI = "dlugoscKolejki";

    private FomObject klientClassHandle;
    private FomObject kasaClassHandle;
    private FomInteraction wejscieDoKolejkiClassHandle;

    private GenerationStrategy<Double> customerGenerationStrategy;

    private KlientFederateAmbassador fedAmbassador;
    private List<Customer> shoppingCustomers;
    private Map<Integer, Integer> checkoutsQueueSizes;
    private Map<Integer, Customer> customersHandlesToObjects;
    private Map<Customer, Integer> customersObjectsToHandles;

    private FomInteraction closeTheMarketClassHandle;

    private FomInteraction stopClassHandle;

    public KlientFederate(String federateName, String federationName, File fomFile)
            throws RTIinternalError, MalformedURLException {
        super(federateName, federationName, fomFile);
        shoppingCustomers = new ArrayList<>();
        checkoutsQueueSizes = new HashMap<>();
        customersHandlesToObjects = new HashMap<>();
        customersObjectsToHandles = new HashMap<>();
        customerGenerationStrategy = GenerationStrategyFactory.randomGenerationStrategy(CUSTOMER_GENERATION_FACTOR);
    }

    public static void main(String[] args) {
        executeFederate(args);
    }

    @Override
    public void runFederate() throws RTIexception {
        StopWatch oneSecondStopwatch = StopWatch.createStopWatchForDuration(LOOP_MINIMAL_DURATION);
        while (isRunning) {
            oneSecondStopwatch.reset();
            double federateTime = getFederateAmbassador().getFederateTime();
            logger.info("[{}] {}", federateTime, this);
            optionallyCreateNewCustomer(federateTime);
            updateCustomersWithNewFederateTime(federateTime);
            Collection<Customer> customersDoneShopping = getCustomersDoneShopping();
            logger.info("{} customer(s) finished shopping", customersDoneShopping.size());
            customersDoneShopping.forEach(customer -> {
                Optional<Entry<Integer, Integer>> min = getCheckoutWithShortestQueue();
                optionallySendQueueEnteredInteraction(customer, min);
                deleteCustomerIfNoCheckoutAvailable(customer, min);
            });
            oneSecondStopwatch.blockUntilDurationPassed();
            advanceTime(1);
        }
    }

    private void deleteCustomerIfNoCheckoutAvailable(Customer customer, Optional<Entry<Integer, Integer>> min) {
        if (!min.isPresent()) {
            logger.info("No checkouts to choose from - customer {} leaves the federation", customer);
            try {
                shoppingCustomers.remove(customer);
                Integer customerHandle = this.customersObjectsToHandles.get(customer);
                this.customersObjectsToHandles.remove(customer);
                this.customersHandlesToObjects.remove(customerHandle);
                rtiAmbassador.deleteObjectInstance(customerHandle, generateTag());
            } catch (Exception e) {
                logger.error("Customer {} couldn't leave the federation (just let him go, please), because: {}",
                        customer,
                        e.getMessage());
            }
        }
    }

    private Optional<Entry<Integer, Integer>> getCheckoutWithShortestQueue() {
        Optional<Entry<Integer, Integer>> min = checkoutsQueueSizes.entrySet().stream().min((entry1, entry2) -> {
            return entry1.getValue() - entry2.getValue();
        });
        return min;
    }

    private void optionallySendQueueEnteredInteraction(Customer customer, Optional<Entry<Integer, Integer>> min) {
        min.ifPresent(entry -> {
            logger.info("Customer {} entering queue in checkout {}", customer, entry.getKey());
            entry.setValue(entry.getValue() + 1);
            sendQueueEnteredInteraction(this.customersObjectsToHandles.get(customer),
                    entry.getKey(),
                    customer.isPriviledged());
            this.shoppingCustomers.remove(customer);
        });
    }

    private void sendQueueEnteredInteraction(Integer customerObjectId, Integer checkoutObjectId, boolean priviledged) {
        SuppliedParameters parameters;
        try {
            parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
            parameters.add(wejscieDoKolejkiClassHandle.getHandleFor(ID_KLIENT),
                    EncodingHelpers.encodeInt(customerObjectId));
            parameters.add(wejscieDoKolejkiClassHandle.getHandleFor(ID_KASA),
                    EncodingHelpers.encodeInt(checkoutObjectId));
            parameters.add(wejscieDoKolejkiClassHandle.getHandleFor(UPRZYWILEJOWANY),
                    EncodingHelpers.encodeBoolean(priviledged));
            rtiAmbassador.sendInteraction(wejscieDoKolejkiClassHandle.getClassHandle(), parameters, generateTag());
        } catch (RTIexception e) {
            logger.error("Couldn't send queue entered interaction, because: {}", e.getMessage());
        }
    }

    private Collection<Customer> getCustomersDoneShopping() {
        return shoppingCustomers.stream().filter(Customer::hasFinishedShopping).collect(Collectors.toList());
    }

    private void updateCustomersWithNewFederateTime(double newFederateTime) {
        shoppingCustomers.forEach(customer -> {
            customer.updateWithNewFederateTime(newFederateTime);
        });
    }

    private void optionallyCreateNewCustomer(double oldFederateTime) {
        if (customerGenerationStrategy.shouldGenerate(oldFederateTime)) {
            createAndRegisterCustomer(oldFederateTime);
        }
    }

    private void createAndRegisterCustomer(double oldFederateTime) {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        Customer customer = new Customer(oldFederateTime,
                (double) current.nextInt(MAX_SHOPPING_TIME - MIN_SHOPPING_TIME + 1) + MIN_SHOPPING_TIME, null);
        customer.setPriviledged(current.nextBoolean());
        shoppingCustomers.add(customer);
        try {
            int customerHandle = registerRtiCustomer(customer);
            logger.info("New customer {} enters the market: {}", customerHandle, customer);
        } catch (RTIexception e) {
            logger.error("Couldn't create new customer, because: {}", e.getMessage(), e);
        }
    }

    private int registerRtiCustomer(Customer customer) throws RTIexception {
        int customerHandle = rtiAmbassador.registerObjectInstance(klientClassHandle.getClassHandle());
        SuppliedAttributes attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();
        attributes.add(klientClassHandle.getHandleFor(CZAS_ZAKUPOW),
                EncodingHelpers.encodeDouble(customer.getShoppingTime()));
        attributes.add(klientClassHandle.getHandleFor(UPRZYWILEJOWANY),
                EncodingHelpers.encodeBoolean(customer.isPriviledged()));
        rtiAmbassador.updateAttributeValues(customerHandle, attributes, generateTag());
        customersHandlesToObjects.put(customerHandle, customer);
        customersObjectsToHandles.put(customer, customerHandle);
        return customerHandle;
    }

    private static void executeFederate(String[] args) {
        String federateName = "klientFederate";
        String federationName = AbstractFederateAmbassador.FEDERATION_NAME;
        String pathname = AbstractFederate.FOM_PATH;
        if (args.length > 0) {
            federateName = args[0];
        }
        if (args.length > 1) {
            federationName = args[1];
        }
        if (args.length > 2) {
            pathname = args[2];
        }
        try {
            logger.info("Creating federate");
            Federate federate = new KlientFederate(federateName, federationName, new File(pathname));
            federate.initiateFederate();
            federate.runFederate();
            federate.cleanUpFederate();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RTIexception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected AbstractFederateAmbassador getFederateAmbassador() {
        return fedAmbassador;
    }

    @Override
    protected void prepareFederationAmbassador() {
        fedAmbassador = new KlientFederateAmbassador();
        fedAmbassador.registerObjectInstanceCreatedListener((objectHandle, classHandle, objectName) -> {
            objectToClassHandleMap.put(objectHandle, classHandle);
            checkoutsQueueSizes.put(objectHandle, 0);
        });
        fedAmbassador
                .registerAttributesUpdatedListener((objectHandle, theAttributes, tag, theTime, retractionHandle) -> {
                    Integer classHandle = objectToClassHandleMap.get(objectHandle);
                    if (classHandle.equals(kasaClassHandle.getClassHandle())) {
                        logger.info("Checkout {} updated, updating queue size", objectHandle);
                        for (int i = 0; i < theAttributes.size(); i++) {
                            int attributeHandle;
                            try {
                                attributeHandle = theAttributes.getAttributeHandle(i);
                                Class<?> typeFor = kasaClassHandle.getTypeFor(attributeHandle);
                                if (isAttributeQueueLength(attributeHandle, typeFor)) {
                                    updateCheckoutQueueWith(objectHandle, theAttributes.getValue(i));
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    }
                });
        fedAmbassador.registerInteractionReceivedListener((int interactionClass, ReceivedInteraction theInteraction,
                byte[] tag, LogicalTime theTime, EventRetractionHandle eventRetractionHandle) -> {
            if (interactionClass == closeTheMarketClassHandle.getClassHandle()) {
                this.customerGenerationStrategy = GenerationStrategyFactory.nopGenerationStrategy();
            } else if (interactionClass == stopClassHandle.getClassHandle()) {
                logger.info("Stop interaction received");
                stop();
            }
        });
    }

    private void updateCheckoutQueueWith(int objectHandle, byte[] value) throws ArrayIndexOutOfBounds {
        checkoutsQueueSizes.put(objectHandle, EncodingHelpers.decodeInt(value));
    }

    private boolean isAttributeQueueLength(int attributeHandle, Class<?> typeFor) {
        return typeFor.getName().equalsIgnoreCase(Integer.class.getName())
                && kasaClassHandle.getNameFor(attributeHandle).equalsIgnoreCase(DLUGOSC_KOLEJKI);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishAndSubscribe() throws RTIexception {
        klientClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Klient"),
                new Pair<String, Class<?>>(ID, Integer.class),
                new Pair<String, Class<?>>(CZAS_ZAKUPOW, Integer.class),
                new Pair<String, Class<?>>(UPRZYWILEJOWANY, Boolean.class),
                new Pair<String, Class<?>>(NR_W_KOLEJCE, Integer.class),
                new Pair<String, Class<?>>(NR_KASY, Integer.class));
        rtiAmbassador.publishObjectClass(klientClassHandle.getClassHandle(),
                klientClassHandle.createAttributeHandleSet());

        wejscieDoKolejkiClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.WejscieDoKolejki"),
                new Pair<String, Class<?>>(ID_KLIENT, Integer.class),
                new Pair<String, Class<?>>(ID_KASA, Integer.class),
                new Pair<String, Class<?>>(UPRZYWILEJOWANY, Boolean.class));
        rtiAmbassador.publishInteractionClass(wejscieDoKolejkiClassHandle.getClassHandle());

        kasaClassHandle = prepareFomObject(rtiAmbassador.getObjectClassHandle("HLAobjectRoot.Kasa"),
                new Pair<String, Class<?>>(DLUGOSC_KOLEJKI, Integer.class));
        rtiAmbassador.subscribeObjectClassAttributes(kasaClassHandle.getClassHandle(),
                kasaClassHandle.createAttributeHandleSet());

        closeTheMarketClassHandle = prepareFomInteraction(
                rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.CloseMarket"));
        rtiAmbassador.subscribeInteractionClass(closeTheMarketClassHandle.getClassHandle());

        stopClassHandle = prepareFomInteraction(rtiAmbassador.getInteractionClassHandle("HLAinteractionRoot.Stop"));
        rtiAmbassador.subscribeInteractionClass(stopClassHandle.getClassHandle());
    }

    @Override
    protected void deleteObjects() throws RTIexception {
        logger.info("Deleting {} created customer objects", customersHandlesToObjects.size());
        customersHandlesToObjects.keySet().stream().forEach(handle -> {
            try {
                rtiAmbassador.deleteObjectInstance(handle, generateTag());
            } catch (Exception e) {
                logger.error("Couldn't delete {}, because: {}", handle, e.getMessage());
            }
        });
    }

    @Override
    public String toString() {
        return "KlientFederate [shoppingCustomers[" + shoppingCustomers.size() + "], checkoutsQueueSizes["
                + checkoutsQueueSizes.size() + "]=" + checkoutsQueueSizes + "]";
    }
}
