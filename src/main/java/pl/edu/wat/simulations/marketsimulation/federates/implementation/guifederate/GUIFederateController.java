package pl.edu.wat.simulations.marketsimulation.federates.implementation.guifederate;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import pl.edu.wat.simulations.marketsimulation.helpers.Pair;

public class GUIFederateController implements Initializable {

    @FXML
    Button achieveSyncPoint;
    @FXML
    Text federationStatus;
    @FXML
    Label shoppingCustomers;
    @FXML
    Label avgShoppingTime;
    @FXML
    Label avgWaitingTime;
    @FXML
    Label avgServiceTime;
    private Map<Integer, Integer> checkoutToQueueSizeMap = new HashMap<>();
    @FXML
    ListView<Text> openCheckouts;
    private ObservableList<Text> checkouts = FXCollections.observableArrayList();
    private Map<Integer, Integer> checkoutObjectHandlesToListIndex = new HashMap<>();
    @FXML
    Label customersLeft;
    @FXML
    Button stop;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        openCheckouts.setItems(checkouts);
        stop.setOnAction(event -> {
            Platform.runLater(() -> {
                Platform.exit();
            });
        });
    }

    public void updateCheckouts(int theObject, Pair<Integer, Boolean> value) {
        if (!checkoutToQueueSizeMap.containsKey(theObject)) {
            Text text = new Text();
            text.setText(getCheckoutString(theObject, value));
            if (value.getB()) {
                text.setFill(Color.RED);
            }
            checkoutObjectHandlesToListIndex.put(theObject, checkouts.size());
            Platform.runLater(() -> {
                checkouts.add(text);
            });
        } else {
            Integer listId = checkoutObjectHandlesToListIndex.get(theObject);
            Platform.runLater(() -> {
                Text text = checkouts.get(listId);
                text.setText(getCheckoutString(theObject, value));
                if (value.getB()) {
                    text.setFill(Color.RED);
                } else {
                    text.setFill(Color.BLACK);
                }
            });
        }
        checkoutToQueueSizeMap.put(theObject, value.getA());
    }

    private String getCheckoutString(int theObject, Pair<Integer, Boolean> value) {
        return theObject + "(" + value.getA() + ")";
    }

    public void registerMainButtonAction(Runnable runnable) {
        Platform.runLater(() -> {
            achieveSyncPoint.setOnAction(event -> {
                runnable.run();
            });
        });
    }

    public void registerStopButtonAction(Runnable runnable) {
        Platform.runLater(() -> {
            stop.setOnAction(event -> {
                runnable.run();
            });
        });
    }

    public void changeMainButtonText(String string) {
        Platform.runLater(() -> {
            achieveSyncPoint.setText(string);
        });
    }

    public void disableMainButton() {
        Platform.runLater(() -> {
            achieveSyncPoint.setDisable(true);
        });
    }

    public void enableMainButton() {
        Platform.runLater(() -> {
            achieveSyncPoint.setDisable(false);
        });
    }

    public void setFederationStatus(String status) {
        Platform.runLater(() -> {
            federationStatus.setText(status);
        });
    }

    public void setShoppingCustomers(int amount) {
        updateLabel(amount + "", shoppingCustomers);
    }

    public void setAvgShoppingTime(double value) {
        updateLabel(new DecimalFormat("#.##").format(value), avgShoppingTime);
    }

    public void setAvgWaitingTime(double value) {
        updateLabel(new DecimalFormat("#.##").format(value), avgWaitingTime);
    }

    public void setAvgServiceTime(double value) {
        updateLabel(new DecimalFormat("#.##").format(value), avgServiceTime);
    }

    public void updateLabel(String text, Label label) {
        Platform.runLater(() -> {
            label.setText(text);
        });
    }

    class CheckoutTableData {
        private int theObject;
        private int queueSize;
        private boolean filled;

        public CheckoutTableData(int theObject, int queueSize, boolean filled) {
            this.theObject = theObject;
            this.queueSize = queueSize;
            this.filled = filled;
        }

        public int getTheObject() {
            return theObject;
        }

        public void setTheObject(int theObject) {
            this.theObject = theObject;
        }

        public int getQueueSize() {
            return queueSize;
        }

        public void setQueueSize(int queueSize) {
            this.queueSize = queueSize;
        }

        public boolean isFilled() {
            return filled;
        }

        public void setFilled(boolean filled) {
            this.filled = filled;
        }

        @Override
        public String toString() {
            return "CheckoutTableData [theObject=" + theObject + ", queueSize=" + queueSize + ", filled=" + filled
                    + "]";
        }
    }

    public void setCustomersLeft(int customersLeft) {
        updateLabel(customersLeft + "", this.customersLeft);
    }

    public void disableStopButton() {
        Platform.runLater(() -> {
            stop.setDisable(true);
        });
    }
}
